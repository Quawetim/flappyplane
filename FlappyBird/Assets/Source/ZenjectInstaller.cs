using Source.Entity;
using Source.Managers.Audio;
using Source.Managers.Game;
using Source.Managers.Input;
using Source.Managers.Score;
using Source.Signals;
using UnityEngine;
using Zenject;

namespace Source
{
    [CreateAssetMenu(fileName = "ZenjectInstaller", menuName = "ZenjectInstaller")]
    public class ZenjectInstaller : ScriptableObjectInstaller<ZenjectInstaller>
    {
        public AudioManager.SoundDescription[] SoundDescriptions;

        public GameObject ObstaclePrefab;

        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<OnDeathSignal>();
            Container.DeclareSignal<OnGameStartSignal>();
            Container.DeclareSignal<OnPauseSignal>();
            Container.DeclareSignal<OnResumeSignal>();
            Container.DeclareSignal<OnClickSignal>();

            BindAudioManager();

            Container.BindInterfacesAndSelfTo<GameStateManager>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<ScoreManager>()
                .AsSingle()
                .NonLazy();

            BindLevelManager();

            Container.BindInterfacesAndSelfTo<InputManager>()
                .FromNewComponentOnNewGameObject()
                .WithGameObjectName("InputManager")
                .AsSingle()
                .NonLazy();
        }

        private void BindLevelManager()
        {
            Container.BindMemoryPool<Obstacle, Obstacle.Pool>()
                .WithInitialSize(3)
                .FromComponentInNewPrefab(ObstaclePrefab);

            Container.BindInterfacesAndSelfTo<LevelManager>()
                .AsSingle()
                .NonLazy();
        }

        private void BindAudioManager()
        {
            Container.DeclareSignal<PlaySoundSignal>();
            Container.DeclareSignal<MuteSoundSignal>();

            Container.BindInterfacesAndSelfTo<AudioManager>()
                .FromNewComponentOnNewGameObject()
                .WithGameObjectName("AudioManager")
                .AsSingle()
                .NonLazy();

            Container.BindInstance(SoundDescriptions)
                .WhenInjectedInto<AudioManager>();
        }
    }
}