﻿using Source.Managers.Game;
using UnityEngine;
using Zenject;

namespace Source
{
    [CreateAssetMenu(fileName = "GameManagerInstaller", menuName = "GameManagerInstaller")]
    public class GameManagerInstaller : ScriptableObjectInstaller<GameManagerInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameManager>()
                .AsSingle()
                .NonLazy();
        }
    }
}