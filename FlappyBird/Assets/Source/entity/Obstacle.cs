using UnityEngine;
using Zenject;

namespace Source.Entity
{
    public class Obstacle : MonoBehaviour
    {
        [SerializeField]
        private GameObject _upperRock;

        [SerializeField]
        private GameObject _lowerRock;

        public void Move(float distance)
        {
            transform.localPosition += Vector3.left * distance;
        }

        public void Reinitialize(float positionX, float yUp, float yLow)
        {
            SetPosition(positionX, yUp, yLow);

            gameObject.SetActive(true);
        }

        public void OnDespawned()
        {
            gameObject.SetActive(false);
        }

        public void SetPosition(float positionX, float yUp, float yLow)
        {
            _upperRock.transform.localPosition = new Vector3(0, yUp, 0);
            _lowerRock.transform.localPosition = new Vector3(0, yLow, 0);

            transform.localPosition = new Vector3(positionX, 0, 0);
        }

        public Vector3 Position => transform.localPosition;

        public class Pool : MonoMemoryPool<float, float, float, Obstacle>
        {
            protected override void Reinitialize(float positionX, float yUp, float yLow, Obstacle item)
            {
                base.Reinitialize(positionX, yUp, yLow, item);
                item.Reinitialize(positionX, yUp, yLow);
            }

            protected override void OnDespawned(Obstacle item)
            {
                base.OnDespawned(item);
                item.OnDespawned();
            }
        }
    }
}