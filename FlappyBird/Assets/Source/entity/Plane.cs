﻿#pragma warning disable 649

using System;
using Source.Managers.Audio;
using Source.Managers.Game;
using Source.Managers.Input;
using Source.Managers.Score;
using Source.Signals;
using UnityEngine;
using Zenject;

namespace Source.Entity
{
    public class Plane : MonoBehaviour, IDisposable
    {
        [SerializeField]
        private Rigidbody2D _rigidBody;

        private SignalBus _signalBus;
        private GameStateManager _gameStateManager;
        private InputManager _inputManager;
        private ScoreManager _scoreManager;

        private const float ELEVATION_FORCE = 70.0f;
        private const float PLANE_DEATH_Y = 60.0f;

        [Inject]
        public void Cosntruct(SignalBus signalBus, GameStateManager gameStateManager, InputManager inputManager, ScoreManager scoreManager)
        {
            _signalBus = signalBus;
            _gameStateManager = gameStateManager;
            _inputManager = inputManager;
            _scoreManager = scoreManager;

            _rigidBody.bodyType = RigidbodyType2D.Static;

            _signalBus.Subscribe<OnPauseSignal>(OnPause);
            _signalBus.Subscribe<OnResumeSignal>(OnResume);
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<OnPauseSignal>(OnPause);
            _signalBus.TryUnsubscribe<OnResumeSignal>(OnResume);
        }

        private void OnDestroy()
        {
            Dispose();
        }

        private void Update()
        {
            switch (_gameStateManager.GameState)
            {
                case GameState.Ready:
                {
                    if (_inputManager.LMBPressed())
                    {
                        _rigidBody.bodyType = RigidbodyType2D.Dynamic;

                        LiftUp();

                        _signalBus.Fire(new OnGameStartSignal());
                    }
                    break;
                }
                case GameState.InGame:
                {
                    if (_inputManager.LMBPressed())
                    {
                        LiftUp();
                    }

                    var topBorder = _rigidBody.position.y > PLANE_DEATH_Y;
                    var bottomBorder = _rigidBody.position.y < -PLANE_DEATH_Y;

                    if (topBorder || bottomBorder)
                    {
                        Die();
                    }

                    transform.eulerAngles = new Vector3(0, 0, _rigidBody.velocity.y * 0.1f);

                    break;
                }
                default:
                {
                    break;
                }
            }
        }

        private void LiftUp()
        {
            if (_gameStateManager.GameState == GameState.Ready || _gameStateManager.GameState == GameState.InGame)
            {
                if (_rigidBody.bodyType == RigidbodyType2D.Dynamic)
                {
                    _rigidBody.velocity = Vector2.up * ELEVATION_FORCE;

                    Debug.Assert(_rigidBody.velocity.x == 0);
                    Debug.Assert(_rigidBody.velocity.y > 0);
                }

                _signalBus.Fire(new PlaySoundSignal { SoundType = AudioManager.Sound.LiftUp });
            }
        }

        private void Die()
        {
            _gameStateManager.SetGameState(GameState.GameOver);

            _scoreManager.UpdateBestScore();

            _rigidBody.bodyType = RigidbodyType2D.Static;

            _signalBus.Fire(new PlaySoundSignal { SoundType = AudioManager.Sound.Death });

            _signalBus.Fire(new OnDeathSignal());
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Die();
        }

        private void OnPause()
        {
            _rigidBody.bodyType = RigidbodyType2D.Static;
        }

        private void OnResume()
        {
            _rigidBody.bodyType = RigidbodyType2D.Dynamic;
        }
    }
}