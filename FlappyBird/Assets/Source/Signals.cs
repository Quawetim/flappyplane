﻿using Source.Managers.Audio;

namespace Source.Signals
{
    /// <summary>Plane death signal.</summary>
    public struct OnDeathSignal { }

    /// <summary>Game start signal.</summary>
    public struct OnGameStartSignal { }

    /// <summary>Game pause signal.</summary>
    public struct OnPauseSignal { }

    /// <summary>Game resume signal.</summary>
    public struct OnResumeSignal { }

    /// <summary>Button click signal.</summary>
    public struct OnClickSignal { }

    public struct PlaySoundSignal
    {
        public AudioManager.Sound SoundType;
    }

    public struct MuteSoundSignal
    {
        public bool IsMuted;
    }
}