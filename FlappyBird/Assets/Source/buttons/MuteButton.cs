﻿#pragma warning disable 649

using Source.Managers.Audio;
using Source.Signals;
using Zenject;

namespace Source.UI.Buttons
{
    /// <summary>Button to mute sound on setting screen.</summary>
    public class MuteButton : UIButton
    {
        private AudioManager _audioManager;

        [Inject]
        public void SubConstruct(AudioManager audioManager)
        {
            _audioManager = audioManager;
        }

        protected override void OnClick()
        {
            base.OnClick();

            _audioManager.MuteSound(!_audioManager.IsMuted);

            if (!_audioManager.IsMuted)
            {
                _signalBus.Fire(new PlaySoundSignal { SoundType = AudioManager.Sound.Click });
            }
        }
    }
}
