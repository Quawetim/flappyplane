#pragma warning disable 649

using System;
using Source.Managers.Audio;
using Source.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Source.UI.Buttons
{
    [RequireComponent(typeof(Button))]
    public abstract class UIButton : MonoBehaviour, IDisposable
    {
        protected SignalBus _signalBus;
        protected Button _button;

        [Inject]
        public void Construct(SignalBus signalBus)
        {
            _signalBus = signalBus;

            _button = gameObject.GetComponent<Button>();

            Initialize();
        }

        public virtual void Initialize()
        {
            _button?.onClick.AddListener(OnClick);
        }

        public virtual void Dispose()
        {
            _button?.onClick.RemoveAllListeners();
        }

        protected virtual void OnDestroy()
        {
            Dispose();
        }

        protected virtual void OnClick()
        {
            _signalBus.Fire(new PlaySoundSignal { SoundType = AudioManager.Sound.Click });
        }
    }
}