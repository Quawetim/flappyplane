﻿#pragma warning disable 649

namespace Source.UI.Buttons
{
    /// <summary>Button to show best score screen.</summary>
    public class ScoreButton : UIButton
    {
        protected override void OnClick()
        {
            base.OnClick();

            UnityEngine.SceneManagement.SceneManager.LoadScene("Score");
        }
    }
}