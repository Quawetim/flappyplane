﻿#pragma warning disable 649

using UnityEngine;

namespace Source.UI.Buttons
{
    /// <summary>Button to exit application.</summary>
    public class ExitButton : UIButton
    {
        protected override void OnClick()
        {
            base.OnClick();

            Application.Quit();
        }
    }
}
