﻿#pragma warning disable 649

using Source.Managers.Game;
using Zenject;

namespace Source.UI.Buttons
{
    /// <summary>Button to pause game.</summary>
    public class PauseButton : UIButton
    {
        private GameStateManager _gameStateManager;

        [Inject]
        public void SubConstruct(GameStateManager gameStateManager)
        {
            _gameStateManager = gameStateManager;
        }

        protected override void OnClick()
        {
            base.OnClick();

            _gameStateManager.SetGameState(GameState.Pause);
        }
    }
}
