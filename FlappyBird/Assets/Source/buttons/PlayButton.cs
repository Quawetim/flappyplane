﻿#pragma warning disable 649

namespace Source.UI.Buttons
{
    /// <summary> Play/start game button. </summary>
    public class PlayButton : UIButton
    {
        protected override void OnClick()
        {
            base.OnClick();

            UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        }
    }
}
