﻿#pragma warning disable 649

namespace Source.UI.Buttons
{
    /// <summary>Button to show settings screen.</summary>
    public class SettingsButton : UIButton
    {
        protected override void OnClick()
        {
            base.OnClick();

            UnityEngine.SceneManagement.SceneManager.LoadScene("Settings");
        }
    }
}