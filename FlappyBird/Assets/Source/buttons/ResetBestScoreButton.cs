﻿#pragma warning disable 649

using Source.Managers.Score;
using Zenject;

namespace Source.UI.Buttons
{
    /// <summary>Button to reset best score on best score screen.</summary>
    public class ResetBestScoreButton : UIButton
    {
        private ScoreManager _scoreManager;

        [Inject]
        public void SubConstruct(ScoreManager scoreManager)
        {
            _scoreManager = scoreManager;
        }

        protected override void OnClick()
        {
            base.OnClick();

            _scoreManager.ResetBestScore();
        }
    }
}
