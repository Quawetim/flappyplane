﻿#pragma warning disable 649

namespace Source.UI.Buttons
{
    /// <summary>Button to show main menu.</summary>
    public class ShowMenuButton : UIButton
    {
        protected override void OnClick()
        {
            base.OnClick();

            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        }
    }
}