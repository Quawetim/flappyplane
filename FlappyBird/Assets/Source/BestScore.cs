﻿#pragma warning disable 649

using Source.Managers.Score;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Source.UI
{
    public class BestScore : MonoBehaviour
    {
        private ScoreManager _scoreManager;

        [SerializeField]
        private Text _value;

        [Inject]
        public void Construct(ScoreManager scoreManager)
        {
            _scoreManager = scoreManager;
        }

        void Update()
        {
            _value.text = _scoreManager.BestScore.ToString();
        }
    }
}