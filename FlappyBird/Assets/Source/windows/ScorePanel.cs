﻿#pragma warning disable 649

using System;
using Source.Managers.Score;
using Source.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Source.UI
{
    public class ScorePanel : MonoBehaviour, IDisposable
    {
        private SignalBus _signalBus;
        private ScoreManager _scoreManager;

        [SerializeField]
        private Text _scoreValue;

        [Inject]
        public void Construct(SignalBus signalBus, ScoreManager scoreManager)
        {
            _signalBus = signalBus;
            _scoreManager = scoreManager;

            _signalBus.Subscribe<OnDeathSignal>(Plane_OnDeath);
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<OnDeathSignal>(Plane_OnDeath);
        }

        private void Update()
        {
            _scoreValue.text = _scoreManager.Score.ToString();
        }

        /// <summary>
        /// Invokes on plane death. Hides score panel.
        /// </summary>
        private void Plane_OnDeath()
        {
            gameObject.SetActive(false);
        }
    }
}