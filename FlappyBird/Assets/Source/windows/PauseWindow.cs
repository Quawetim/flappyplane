﻿#pragma warning disable 649

using System;
using Source.Signals;
using UnityEngine;
using Zenject;

namespace Source.UI.Windows
{
    public class PauseWindow : MonoBehaviour, IDisposable
    {
        private SignalBus _signalBus;

        [Inject]
        public void Construct(SignalBus signalBus)
        {
            _signalBus = signalBus;

            Hide();

            _signalBus.Subscribe<OnPauseSignal>(OnPause);
            _signalBus.Subscribe<OnResumeSignal>(OnResume);
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<OnPauseSignal>(OnPause);
            _signalBus.TryUnsubscribe<OnResumeSignal>(OnResume);
        }

        private void OnDestroy()
        {
            Dispose();
        }

        /// <summary>
        /// Invokes on pause.
        /// </summary>
        private void OnPause()
        {
            Show();
        }

        /// <summary>
        /// Invokes on resume.
        /// </summary>
        private void OnResume()
        {
            Hide();
        }

        /// <summary>
        /// Function to show pause window.
        /// </summary>
        private void Show()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Function to hide pause window.
        /// </summary>
        private void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}