﻿#pragma warning disable 649

using System;
using Source.Managers.Score;
using Source.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Source.UI.Windows
{
    public class GameOverWindow : MonoBehaviour, IDisposable
    {
        private SignalBus _signalBus;
        private ScoreManager _scoreManager;

        [SerializeField]
        private Text _scoreValue;

        [SerializeField]
        private Text _scoreMaxValue;

        [Inject]
        public void Construct(SignalBus signalBus, ScoreManager scoreManager)
        {
            _signalBus = signalBus;
            _scoreManager = scoreManager;

            Hide();

            _signalBus.Subscribe<OnDeathSignal>(Plane_OnDeath);
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<OnDeathSignal>(Plane_OnDeath);
        }

        private void OnDestroy()
        {
            Dispose();
        }

        /// <summary>
        /// Invokes on plane death. Updates best score if needed. Shows game over window.
        /// </summary>
        private void Plane_OnDeath()
        {
            _scoreValue.text = _scoreManager.Score.ToString();
            _scoreMaxValue.text = _scoreManager.BestScore.ToString();

            Show();
        }

        /// <summary>
        /// Function to show game over window.
        /// </summary>
        private void Show()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Function to hide game over window.
        /// </summary>
        private void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}