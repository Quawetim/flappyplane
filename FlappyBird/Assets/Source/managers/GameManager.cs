﻿using System;
using Source.Signals;
using Zenject;

namespace Source.Managers.Game
{
    public sealed class GameManager : IInitializable, IDisposable
    {
        private SignalBus _signalBus;
        private GameStateManager _gameStateManager;
        private LevelManager _levelManager;

        public GameManager(SignalBus signalBus, GameStateManager gameStateManager, LevelManager levelManager)
        {
            _signalBus = signalBus;
            _gameStateManager = gameStateManager;
            _levelManager = levelManager;
        }

        public void Initialize()
        {
            _levelManager.SetDifficulty(Difficulty.Easy);
            _gameStateManager.SetGameState(GameState.Ready);

            _signalBus.Subscribe<OnDeathSignal>(Plane_OnDeath);
            _signalBus.Subscribe<OnGameStartSignal>(Plane_OnGameStart);
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<OnDeathSignal>(Plane_OnDeath);
            _signalBus.TryUnsubscribe<OnGameStartSignal>(Plane_OnGameStart);
        }

        /// <summary>
        /// Function to handle plane death event. Changes gamestate to "GameOver".
        /// </summary>
        private void Plane_OnDeath()
        {
            _gameStateManager.SetGameState(GameState.GameOver);
        }

        /// <summary>
        /// Function to handle game start. Changes gamestate to "InGame".
        /// </summary>
        private void Plane_OnGameStart()
        {
            _gameStateManager.SetGameState(GameState.InGame);
        }
    }
}