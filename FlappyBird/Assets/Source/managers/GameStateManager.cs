﻿#pragma warning disable 649

using Source.Signals;
using Zenject;

namespace Source.Managers.Game
{
    public sealed class GameStateManager
    {
        private SignalBus _signalBus;

        /// <summary>Current game state.</summary>
        private GameState _gameState;

        public GameStateManager(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        /// <summary>
        /// Function to set current game state.
        /// </summary>
        /// <returns>Game state.</returns>
        public void SetGameState(GameState state)
        {
            var previousState = _gameState;
            _gameState = state;

            switch (_gameState)
            {
                default:
                {
                    break;
                }
                case GameState.InGame:
                {
                    if (previousState == GameState.Pause)
                    {
                        _signalBus.Fire(new OnResumeSignal());
                    }

                    break;
                }
                case GameState.Pause:
                {
                    _signalBus.Fire(new OnPauseSignal());

                    break;
                }
            }
        }

        /// <summary>
        /// Function to get current game state.
        /// </summary>
        /// <returns>Game state.</returns>
        public GameState GameState => _gameState;
    }

    public enum GameState
    {
        Ready,
        InGame,
        Pause,
        GameOver
    }

    public enum Difficulty
    {
        Easy,
        Medium,
        Hard,
        DarkSouls
    }
}