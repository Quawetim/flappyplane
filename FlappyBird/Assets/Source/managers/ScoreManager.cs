﻿#pragma warning disable 649

using Source.Managers.Game;
using UnityEngine;

namespace Source.Managers.Score
{
    public class ScoreManager
    {
        private LevelManager _levelManager;

        public ScoreManager(LevelManager levelManager)
        {
            _levelManager = levelManager;
        }

        /// <summary>
        /// Function to check and update if score > best score.
        /// </summary>
        public void UpdateBestScore()
        {
            if (Score > BestScore)
            {
                BestScore = Score;
            }
        }

        /// <summary>
        /// Function to reset to 0 best score in PlayerPrefs.
        /// </summary>
        public void ResetBestScore()
        {
            PlayerPrefs.SetInt("BestScore", 0);
        }

        /// <summary>
        /// Returns game score.
        /// </summary>
        public int Score => _levelManager.ObstaclesPassed;

        /// <summary>
        /// Saves/loads best score to/from PlayerPrefs.
        /// </summary>
        public int BestScore
        {
            get
            {
                return PlayerPrefs.GetInt("BestScore");
            }

            set
            {
                PlayerPrefs.SetInt("BestScore", value);
            }
        }
    }
}