﻿#pragma warning disable 649

using UnityEngine;
using Zenject;

namespace Source.Managers.Input
{
    public class InputManager : MonoBehaviour, ITickable
    {
        public void Tick()
        {
            if (ShouldExit())
            {
                Application.Quit();
            }
        }

        /// <summary>Function to check exit condition.</summary>
        /// <returns>True if should exit.</returns>
        public bool ShouldExit()
        {
            return UnityEngine.Input.GetKey(KeyCode.Escape);
        }

        /// <summary>Function to check left mouse button pressed.</summary>
        /// <returns>True if LMB pressed.</returns>
        public bool LMBPressed()
        {
            return UnityEngine.Input.GetMouseButtonDown(0);
        }
    }
}