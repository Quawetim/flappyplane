﻿using System;
using System.Linq;
using Source.Signals;
using UnityEngine;
using Zenject;

namespace Source.Managers.Audio
{
    public sealed class AudioManager : MonoBehaviour, IInitializable, IDisposable
    {
        private SignalBus _signalBus;
        private SoundDescription[] _sounds;

        private bool _muteSound;
        private AudioSource _audioSource;

        [Inject]
        public void Construct(SignalBus signalBus, SoundDescription[] sounds)
        {
            _signalBus = signalBus;
            _sounds = sounds;
        }

        public void Initialize()
        {
            var soundObject = new GameObject("Sound", typeof(AudioSource));
            soundObject.transform.parent = transform;

            _audioSource = soundObject.GetComponent<AudioSource>();

            _signalBus.Subscribe<PlaySoundSignal>(OnPlaySound);
            _signalBus.Subscribe<MuteSoundSignal>(OnMuteSound);
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<PlaySoundSignal>(OnPlaySound);
            _signalBus.TryUnsubscribe<MuteSoundSignal>(OnMuteSound);
        }

        private void OnPlaySound(PlaySoundSignal signal)
        {
            if (_muteSound) return;

            var soundDescription = _sounds.FirstOrDefault(sd => sd.SoundType == signal.SoundType);

            if (soundDescription != null && soundDescription.AudioClip != null)
            {
                _audioSource.PlayOneShot(soundDescription.AudioClip);
            }
        }

        private void OnMuteSound(MuteSoundSignal signal)
        {
            _muteSound = signal.IsMuted;
        }

        public void MuteSound(bool isMuted)
        {
            _muteSound = isMuted;
        }

        public bool IsMuted => _muteSound;

        public enum Sound
        {
            LiftUp,
            Score,
            Death,
            Click
        }

        [Serializable]
        public class SoundDescription
        {
            public Sound SoundType;
            public AudioClip AudioClip;
        }
    }
}