﻿#pragma warning disable 649

using System;
using System.Collections.Generic;
using Source.Entity;
using Source.Managers.Audio;
using Source.Signals;
using UnityEngine;
using Zenject;

namespace Source.Managers.Game
{
    public class LevelManager : ITickable, IInitializable, IDisposable
    {
        private SignalBus _signalBus;
        private GameStateManager _gameStateManager;
        private Obstacle.Pool _obstaclePool;

        /// <summary>
        /// Rock spawn params.
        /// <para/>
        /// Rock minimum Y, heght of the free pass, obstacle spawn spacing. 
        /// </summary>
        private readonly float[,] SPAWN_PARAMS = new float[,]
        {
        { 40.0f, 110.0f, 40.0f },
        { 35.0f, 100.0f, 42.0f },
        { 30.0f, 94.0f, 44.0f },
        { 25.0f, 88.0f, 46.0f }
        };

        /// <summary>How much obstacles need to spawn to change difficulty. From easiest to hardest.</summary>
        private readonly Vector3 DIFFICULTY_PARAMS = new Vector3(10, 25, 45); // 10 25 45

        public float ROCK_MAX_Y = 65.0f;
        public float OBSTACLE_SPEED = 30.0f;
        public float OBSTACLE_SPAWN_X = 50.0f;
        public float PLANE_POSITION_X = 0.0f;

        private int _obstaclesSpawned;
        private int _obstaclesPassed;
        private float _obstacleSpawnSpacing;
        private float _rockMinY;
        private float _passHeight;

        private List<Obstacle> _obstacles = new List<Obstacle>();

        public LevelManager(SignalBus signalBus, GameStateManager gameStateManager, Obstacle.Pool obstaclePool)
        {
            _signalBus = signalBus;
            _gameStateManager = gameStateManager;

            _obstaclePool = obstaclePool;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<OnGameStartSignal>(OnGameStart);
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<OnGameStartSignal>(OnGameStart);
        }

        public void Tick()
        {
            var gameState = _gameStateManager.GameState;

            switch (gameState)
            {
                case GameState.InGame:
                {
                    MoveObstacles();
                    break;
                }
                case GameState.GameOver:
                {
                    DespawnAllObstacles();
                    break;
                }
                default:
                {
                    break;
                }
            }
        }

        public void OnGameStart()
        {
            _obstaclesPassed = 0;

            SetDifficulty(Difficulty.Easy);

            for (var i = 0; i < 3; i++)
            {
                var x = OBSTACLE_SPAWN_X + i * _obstacleSpawnSpacing;
                var yUp = UnityEngine.Random.Range(_rockMinY, ROCK_MAX_Y);
                var yLow = yUp - _passHeight;

                var obstacle = _obstaclePool.Spawn(x, yUp, yLow);
                _obstacles.Add(obstacle);
            }
        }

        private void DespawnAllObstacles()
        {
            foreach (var obstacle in _obstacles)
            {
                _obstaclePool.Despawn(obstacle);
            }

            _obstacles.Clear();
        }

        private void MoveObstacles()
        {
            foreach (var obstacle in _obstacles)
            {
                var notPassedBeforeMove = obstacle.Position.x > PLANE_POSITION_X;

                obstacle.Move(OBSTACLE_SPEED * Time.deltaTime);

                var passedAfterMove = obstacle.Position.x <= PLANE_POSITION_X;

                if (notPassedBeforeMove && passedAfterMove)
                {
                    _obstaclesPassed++;

                    _signalBus.Fire(new PlaySoundSignal { SoundType = AudioManager.Sound.Score });
                }

                var respawn = obstacle.Position.x < -OBSTACLE_SPAWN_X;

                if (respawn)
                {
                    var x = OBSTACLE_SPAWN_X + _obstacleSpawnSpacing;
                    var yUp = UnityEngine.Random.Range(_rockMinY, ROCK_MAX_Y);
                    var yLow = yUp - _passHeight;

                    obstacle.SetPosition(x, yUp, yLow);
                }
            }

            SetDifficulty(GetDifficulty());
        }

        public Difficulty GetDifficulty()
        {
            if (ObstaclesPassed >= DIFFICULTY_PARAMS.z) return Difficulty.DarkSouls;
            if (ObstaclesPassed >= DIFFICULTY_PARAMS.y) return Difficulty.Hard;
            if (ObstaclesPassed >= DIFFICULTY_PARAMS.x) return Difficulty.Medium;

            return Difficulty.Easy;
        }

        public void SetDifficulty(Difficulty difficulty)
        {
            switch (difficulty)
            {
                default:
                case Difficulty.Easy:
                {
                    SetSpawnParams(SPAWN_PARAMS[0, 0], SPAWN_PARAMS[0, 1], SPAWN_PARAMS[0, 2]);
                    break;
                }
                case Difficulty.Medium:
                {
                    SetSpawnParams(SPAWN_PARAMS[1, 0], SPAWN_PARAMS[1, 1], SPAWN_PARAMS[2, 2]);
                    break;
                }
                case Difficulty.Hard:
                {
                    SetSpawnParams(SPAWN_PARAMS[2, 0], SPAWN_PARAMS[2, 1], SPAWN_PARAMS[2, 2]);
                    break;
                }
                case Difficulty.DarkSouls:
                {
                    SetSpawnParams(SPAWN_PARAMS[3, 0], SPAWN_PARAMS[3, 1], SPAWN_PARAMS[3, 2]);
                    break;
                }
            }
        }

        /// <summary>
        /// Function to set obstacles spawn params.
        /// </summary>
        /// <param name="rockMinY">Rock minimum Y.</param>
        /// <param name="passHeight">Heght of the free pass.</param>
        /// <param name="obstacleSpawnSpacing">Widtn between obstacles.</param>
        public void SetSpawnParams(float rockMinY, float passHeight, float obstacleSpawnSpacing)
        {
            _rockMinY = rockMinY;
            _passHeight = passHeight;
            _obstacleSpawnSpacing = obstacleSpawnSpacing;
        }

        public int ObstaclesPassed => _obstaclesPassed;
    }
}